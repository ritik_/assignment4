import java.util.*;
class Player {
	public String name;
	public int score;
	public Player(String pN, int pS)
	{
		name = pN;
		score = pS;
	}
	public int getScore(){return score;}
	public String getName(){return name;}
	public String toString()
	{
		return ("Player - "+name+" has scored : "+ score + " points");
	}
}
public class ScoreCard{
	public int numPlayers;
	Player board[];
	static String first_name_list[] = {"Thomas","Christopher","Daniel","Matthew","George","Anthony","Donald","Paul","Mark","Edward",
					"Andrew","Steven", "Kenneth","Joshua","Kevin","Brian","Ronald","Timothy","Jason","Jeffrey","Ryan",
					"Jacob","Frank","Gary","Nichola","Eric","Stephen","Jonatha","Larry","Justin","Raymond","Scott","Brandon",
					"Samuel","Benjamin","Gregory","Jack","Henry","Patrick","Alexander","Walter"};
	static String last_name_list[] = {" Malfoy"," Potter", " Dobby", " Karakar", " Trump", " Saharam", " Felicis"};

	public ScoreCard(int np, int capacity){
		//Constructor	
		numPlayers = np;
		board = new Player [capacity];			
	}

	public void PrintScore(int a)
	{
		System.out.println(board[a].toString());
		
		//Print the complete leader board
	}
	public void addPlayer(Player p, int s) { 
			board[s] = p;
			PrintScore(s);
			/*if(s <= board.length-1) //leader board not full         sc.printScore
			{				
				board[s] = p;//add player to the leader board
			}
			else
			{
				removePlayer(p,s);// call removePlayer() function
			}*/
	}
	
	public void removePlayer(Player p) {
		int[] res = findMin();
		if(p.getScore() > res[0])//current player score is more than the minimum score on leader board
		{	
			System.out.println("Player "+ (board[res[1]].getName()) + " won't stay in the Leaderboard as his/her score " + (board[res[1]].getScore()) + " is the lowest in the Leaderboard and as a result his/her positon will be taken by \n" + (p.getName()) + "whose score is " + (p.getScore()) + ".");
			board[res[1]] = p;
		}
		else
		{
			System.out.println("Player "+ (p.getName()) + " doesn't qualify to be on the Leaderboard as his/her score " + (p.getScore()) + " is very less as comapred to the Players in the Leaderboard itself.");
		}
	}
	public int[] findMin()
	{
		int minScore = board[0].getScore();
		int minPos=0;
		for(int i=0;i<board.length;i++)
		{
			if(minScore > board[i].getScore())
			{
				minScore = board[i].getScore();
				minPos = i;
			}
		}
		int result[] = {minScore, minPos};
		return result;
	}
	public static void main(String[] args){
		ScoreCard sc = new ScoreCard(15,10);
		Random r = new Random();
		int randomNumber[][] = new int[2][2];
		//int n=sc.numPlayers - sc.board.length;
		System.out.println("Let us assume that there are these following " + sc.board.length + " Players in the Leaderboard.");
		for(int s=0;s<sc.numPlayers;s++)
		{
			randomNumber[1][0]= r.nextInt(first_name_list.length);
        	randomNumber[0][1]= r.nextInt(last_name_list.length);
        	String total = first_name_list[randomNumber[1][0]].concat(last_name_list[randomNumber[0][1]]);
        
        	int rScore = r.nextInt((100 - 0) + 1) + 0;
        
			Player p = new Player(total,rScore);
			if(s<sc.board.length)
			{
				sc.addPlayer(p,s);
			}
			else
			{
				System.out.println("");
				System.out.println("Now we have " + (s+1) + "th Player" + (p.getName()) + " which may or may not join the Leaderboard depending upon his/her score.");
				sc.removePlayer(p);
				System.out.println("");
				System.out.println("Now the new Leaderboard is: ");
				for(int k=0;k<sc.board.length;k++)
					sc.PrintScore(k);
			//addPlayer(p)
			}
		}
	}
}